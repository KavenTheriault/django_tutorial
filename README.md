##### Create project
```
django-admin startproject mysite
```
##### Run development server
```
python manage.py runserver 8000
```
##### Create an app
Django project can have multiple apps
```
python manage.py startapp polls
```
##### Run migration
```
python manage.py migrate
```
##### Make app migration
Look at changes to the app models and store them as migration
```
python manage.py makemigrations polls
```
Print migration SQL
```
python manage.py sqlmigrate polls 0001
```
##### Checks for any problems in your project
```
python manage.py check
```
##### Open python shell with your project environment variable loaded
```
python manage.py shell
```
##### Creating an admin user
```
python manage.py createsuperuser
```
##### Running tests
```
python manage.py test polls
```
##### Notes
- Url mappings are in `site_name/urls.py`, include apps url there
- Django settings are in `site_name/settings.py` (including database settings)
- Remove `DEBUG = True` from `site_name/settings.py` when in production
- Each app need to be in the `INSTALLED_APPS` section of `site_name/settings.py` file
- Models data can be edited in the admin panel. Use `admin.site.register(the_model)` in `app_name/admin.py`
- Must add `{% csrf_token %}` in each html form to avoid `Cross Site Request Forgeries`
##### Play with models
```
Question.objects.all()
Question.objects.filter(id=1)
Question.objects.filter(question_text__startswith='What')
Question.objects.get(pub_date__year=2018)
Question.objects.get(id=1) or Question.objects.get(pk=1)
Question.objects.order_by('-pub_date')[:5] # take 5 items

q = Question.objects.get(pk=1)
q.choice_set.all()
q.choice_set.count()
q.choice_set.create(choice_text='Not much', votes=0)
q.save()
q.delete()
```